package ru.ifmo.math;

import lombok.Getter;
import ru.ifmo.math.logarithm.*;
import ru.ifmo.math.trigonometry.*;

@Getter
public class VeryStrangeMathFunction {

    private final SinFunction sinFunction;
    private final CosFunction cosFunction;
    private final CscFunction cscFunction;
    private final SecFunction secFunction;
    private final TanFunction tanFunction;

    private final LnFunction lnFunction;
    private final Log2Function log2Function;
    private final Log3Function log3Function;
    private final Log10Function log10Function;

    public VeryStrangeMathFunction(SinFunction sinFunction,
                                   CosFunction cosFunction,
                                   CscFunction cscFunction,
                                   SecFunction secFunction,
                                   TanFunction tanFunction,
                                   LnFunction lnFunction,
                                   Log2Function log2Function,
                                   Log3Function log3Function,
                                   Log10Function log10Function) {
        this.sinFunction = sinFunction;
        this.cosFunction = cosFunction;
        this.cscFunction = cscFunction;
        this.secFunction = secFunction;
        this.tanFunction = tanFunction;
        this.lnFunction = lnFunction;
        this.log2Function = log2Function;
        this.log3Function = log3Function;
        this.log10Function = log10Function;
    }

    public double evaluate(Double x) {
        if (x <= 0) {
            double sin = sinFunction.sin(x);
            double cos = cosFunction.cos(x);
            double tan = tanFunction.tan(x);
            double csc = cscFunction.csc(x);
            double sec = secFunction.sec(x);
            return (((((sec - sec) + (sec * sin)) * cos) - (sin * csc)) / tan);
        } else {
            double ln = lnFunction.ln(x);
            double log2 = log2Function.log2(x);
            double log3 = log3Function.log3(x);
            double log10 = log10Function.log10(x);
            return (((((log2 - log2) + (log2 * log10)) * log3) - (log3 * ln)) / log3);
        }
    }
}
