package ru.ifmo.math.logarithm;

import ru.ifmo.math.BasicFunction;

public interface LnFunction extends BasicFunction {
    Double ln(Double x);

    @Override
    default Double evaluate(Double arg) {
        return ln(arg);
    }
}
