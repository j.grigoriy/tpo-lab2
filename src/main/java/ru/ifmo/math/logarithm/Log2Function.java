package ru.ifmo.math.logarithm;

import ru.ifmo.math.BasicFunction;

public interface Log2Function extends BasicFunction {

    Double log2(Double x);

    default Double evaluate(Double arg) {
        return log2(arg);
    }
}
