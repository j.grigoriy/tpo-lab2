package ru.ifmo.math.logarithm;

public class Log10FunctionImpl implements Log10Function {
    private final LnFunction lnFunction;

    public Log10FunctionImpl(LnFunction lnFunction) {
        this.lnFunction = lnFunction;
    }

    public Log10FunctionImpl() {
        this.lnFunction = new LnFunctionImpl();
    }

    @Override
    public Double log10(Double x) {
        return lnFunction.ln(x) / lnFunction.ln(10.0);
    }
}
