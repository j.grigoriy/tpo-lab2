package ru.ifmo.math.logarithm;

import ru.ifmo.math.BasicFunction;

public interface Log3Function extends BasicFunction {

    Double log3(Double x);

    default Double evaluate(Double arg) {
        return log3(arg);
    }
}
