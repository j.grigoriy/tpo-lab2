package ru.ifmo.math.logarithm;

import ru.ifmo.math.BasicFunction;

public interface Log10Function extends BasicFunction {

    Double log10(Double x);

    default Double evaluate(Double arg) {
        return log10(arg);
    }
}
