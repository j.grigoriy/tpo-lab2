package ru.ifmo.math.trigonometry;


public class TanFunctionImpl implements TanFunction {
    private final SinFunction sinFunction;
    private final CosFunction cosFunction;

    public TanFunctionImpl(SinFunction sinFunction, CosFunction cosFunction) {
        this.sinFunction = sinFunction;
        this.cosFunction = cosFunction;
    }

    public TanFunctionImpl() {
        this.sinFunction = new SinFunctionImpl();
        this.cosFunction = new CosFunctionImpl();
    }

    @Override
    public Double tan(Double x) {
        return sinFunction.sin(x) / cosFunction.cos(x);
    }
}
