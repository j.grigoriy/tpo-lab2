package ru.ifmo.math.trigonometry;

import ru.ifmo.math.BasicFunction;

public interface TanFunction extends BasicFunction {
    Double tan(Double x);

    @Override
    default Double evaluate(Double arg) {
        return tan(arg);
    }
}
