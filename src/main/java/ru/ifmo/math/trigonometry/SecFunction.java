package ru.ifmo.math.trigonometry;

import ru.ifmo.math.BasicFunction;

public interface SecFunction extends BasicFunction {
    Double sec(Double x);

    @Override
    default Double evaluate(Double arg) {
        return sec(arg);
    }
}
