package ru.ifmo.math.trigonometry;

import ru.ifmo.math.BasicFunction;

public interface CosFunction extends BasicFunction {
    Double cos(Double x);

    @Override
    default Double evaluate(Double arg) {
        return cos(arg);
    }
}
