package ru.ifmo.math.trigonometry;

import ru.ifmo.math.BasicFunction;

public interface SinFunction extends BasicFunction {
    Double sin(Double x);

    @Override
    default Double evaluate(Double arg) {
        return sin(arg);
    }
}
