package ru.ifmo.math.trigonometry;

import ru.ifmo.math.BasicFunction;

public interface CscFunction extends BasicFunction {
    Double csc(Double x);

    @Override
    default Double evaluate(Double arg) {
        return csc(arg);
    }
}
