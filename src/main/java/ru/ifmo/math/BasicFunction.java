package ru.ifmo.math;

public interface BasicFunction {
    Double evaluate(Double arg);
}
