package ru.ifmo;


import ru.ifmo.math.BasicFunction;
import ru.ifmo.math.logarithm.LnFunctionImpl;
import ru.ifmo.math.logarithm.Log2FunctionImpl;
import ru.ifmo.math.logarithm.Log3FunctionImpl;
import ru.ifmo.math.logarithm.Log10FunctionImpl;
import ru.ifmo.math.trigonometry.SinFunctionImpl;
import ru.ifmo.math.trigonometry.CosFunctionImpl;
import ru.ifmo.math.trigonometry.SecFunctionImpl;
import ru.ifmo.math.trigonometry.CscFunctionImpl;
import ru.ifmo.math.trigonometry.TanFunctionImpl;
import ru.ifmo.util.CsvFilePrinter;

public class Main {
    public static void main(String[] args) {
        CsvFilePrinter.print(new SinFunctionImpl(), -Math.PI, Math.PI, Math.PI / 4, ";", "sin.csv");
        CsvFilePrinter.print(new CosFunctionImpl(), -Math.PI, Math.PI, Math.PI / 4, ";", "cos.csv");
        CsvFilePrinter.print(new SecFunctionImpl(), -Math.PI, Math.PI, Math.PI / 4, ";", "sec.csv");
        CsvFilePrinter.print(new CscFunctionImpl(), -Math.PI, Math.PI, Math.PI / 4, ";", "csc.csv");
        CsvFilePrinter.print(new TanFunctionImpl(), -Math.PI, Math.PI, Math.PI / 4, ";", "tan.csv");
        CsvFilePrinter.print(new LnFunctionImpl(), 0.1, 10.0, 0.1, ";", "ln.csv");
        CsvFilePrinter.print(new Log2FunctionImpl(), 0.1, 10.0, 0.1, ";", "log2.csv");
        CsvFilePrinter.print(new Log3FunctionImpl(), 0.1, 10.0, 0.1, ";", "log3.csv");
        CsvFilePrinter.print(new Log10FunctionImpl(), 0.1, 10.0, 0.1, ";", "log10.csv");

    }
}
