package ru.ifmo;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.ifmo.math.VeryStrangeMathFunction;
import ru.ifmo.math.logarithm.*;
import ru.ifmo.math.trigonometry.*;
import ru.ifmo.stub.logarithm.LnStubImpl;
import ru.ifmo.stub.logarithm.Log10StubImpl;
import ru.ifmo.stub.logarithm.Log2StubImpl;
import ru.ifmo.stub.logarithm.Log3StubImpl;
import ru.ifmo.stub.trigonometry.*;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

public class IntegrationTest {

    private static final double DELTA = 0.01;

    static Stream<Arguments> getTestValues() {
        return Stream.of(
                Arguments.of(-10 * Math.PI / 3, 0.077350269189625764509148780501957455647601751270126876018602326),
                Arguments.of(-Math.PI / 4, 1.7071067811865475244008443621048490392848359376884740365883398689),
                Arguments.of(-2.28 * Math.PI / 99, 14.7946),
                Arguments.of(-666 * Math.PI / 133, 41.327619866543178576165423629335682755279292045932528120959513651),
                Arguments.of(-Math.PI / 8, 3.3380930948843818049298719136064863653920885012405905592917774692),
                Arguments.of(0.8, 0.254342),
                Arguments.of(1.0, Double.NaN),
                Arguments.of(1.2, -0.161494),
                Arguments.of(2.28, -0.398579),
                Arguments.of(Math.E, -0.373445504672736925946453566330817371205355104856916659281678066)
        );
    }

    @ParameterizedTest
    @MethodSource("getTestValues")
    void testStubsOnly(Double arg, Double expected) {
        VeryStrangeMathFunction function = new VeryStrangeMathFunction(
                new SinStubImpl(),
                new CosStubImpl(),
                new CscStubImpl(),
                new SecStubImpl(),
                new TanStubImpl(),
                new LnStubImpl(),
                new Log2StubImpl(),
                new Log3StubImpl(),
                new Log10StubImpl());
        assertEquals(expected, function.evaluate(arg), DELTA);
    }

    @ParameterizedTest
    @MethodSource("getTestValues")
    void testStubsOnBaseFunctions(Double arg, Double expected) {
        SinFunction sinFunction = new SinStubImpl();
        CosFunction cosFunction = new CosFunctionImpl(sinFunction);
        TanFunction tanFunction = new TanFunctionImpl(sinFunction, cosFunction);
        SecFunction secFunction = new SecFunctionImpl(cosFunction);
        CscFunction cscFunction = new CscFunctionImpl(sinFunction);
        LnFunction lnFunction = new LnStubImpl();
        Log2Function log2Function = new Log2FunctionImpl(lnFunction);
        Log3Function log3Function = new Log3FunctionImpl(lnFunction);
        Log10Function log10Function = new Log10FunctionImpl(lnFunction);
        VeryStrangeMathFunction function = new VeryStrangeMathFunction(
                sinFunction,
                cosFunction,
                cscFunction,
                secFunction,
                tanFunction,
                lnFunction,
                log2Function,
                log3Function,
                log10Function);
        assertEquals(expected, function.evaluate(arg), DELTA);
    }

    @ParameterizedTest
    @MethodSource("getTestValues")
    void testWithoutStubs(Double arg, Double expected) {
        SinFunction sinFunction = new SinFunctionImpl();
        CosFunction cosFunction = new CosFunctionImpl(sinFunction);
        TanFunction tanFunction = new TanFunctionImpl(sinFunction, cosFunction);
        SecFunction secFunction = new SecFunctionImpl(cosFunction);
        CscFunction cscFunction = new CscFunctionImpl(sinFunction);
        LnFunction lnFunction = new LnFunctionImpl();
        Log2Function log2Function = new Log2FunctionImpl(lnFunction);
        Log3Function log3Function = new Log3FunctionImpl(lnFunction);
        Log10Function log10Function = new Log10FunctionImpl(lnFunction);
        VeryStrangeMathFunction function = new VeryStrangeMathFunction(
                sinFunction,
                cosFunction,
                cscFunction,
                secFunction,
                tanFunction,
                lnFunction,
                log2Function,
                log3Function,
                log10Function);
        assertEquals(expected, function.evaluate(arg), DELTA);
    }
}
