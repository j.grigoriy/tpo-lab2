package ru.ifmo.stub.logarithm;

import ru.ifmo.math.logarithm.Log2Function;
import ru.ifmo.stub.StubTables;

public class Log2StubImpl implements Log2Function {
    @Override
    public Double log2(Double x) {
        return StubTables.log2ArgToValue.get(x);
    }
}
