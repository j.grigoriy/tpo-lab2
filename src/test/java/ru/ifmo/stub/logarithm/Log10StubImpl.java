package ru.ifmo.stub.logarithm;

import ru.ifmo.math.logarithm.Log10Function;
import ru.ifmo.stub.StubTables;

public class Log10StubImpl implements Log10Function {
    @Override
    public Double log10(Double x) {
        return StubTables.log10ArgToValue.get(x);
    }
}
