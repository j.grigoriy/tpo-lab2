package ru.ifmo.stub.logarithm;

import ru.ifmo.math.logarithm.Log3Function;
import ru.ifmo.stub.StubTables;

public class Log3StubImpl implements Log3Function {
    @Override
    public Double log3(Double x) {
        return StubTables.log3ArgToValue.get(x);
    }
}
