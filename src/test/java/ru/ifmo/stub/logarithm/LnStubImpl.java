package ru.ifmo.stub.logarithm;

import ru.ifmo.math.logarithm.LnFunction;
import ru.ifmo.stub.StubTables;

public class LnStubImpl implements LnFunction {
    @Override
    public Double ln(Double x) {
        return StubTables.lnArgToValue.get(x);
    }
}
