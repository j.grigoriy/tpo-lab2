package ru.ifmo.stub.trigonometry;

import ru.ifmo.math.trigonometry.TanFunction;
import ru.ifmo.stub.StubTables;

public class TanStubImpl implements TanFunction {
    @Override
    public Double tan(Double x) {
        return StubTables.tanArgToValue.get(x);
    }
}
