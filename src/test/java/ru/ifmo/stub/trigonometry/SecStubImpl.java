package ru.ifmo.stub.trigonometry;

import ru.ifmo.math.trigonometry.SecFunction;
import ru.ifmo.stub.StubTables;

public class SecStubImpl implements SecFunction {
    @Override
    public Double sec(Double x) {
        return StubTables.secArgToValue.get(x);
    }
}
