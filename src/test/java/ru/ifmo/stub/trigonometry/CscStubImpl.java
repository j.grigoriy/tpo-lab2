package ru.ifmo.stub.trigonometry;

import ru.ifmo.math.trigonometry.CscFunction;
import ru.ifmo.stub.StubTables;

public class CscStubImpl implements CscFunction {
    @Override
    public Double csc(Double x) {
        return StubTables.cscArgToValue.get(x);
    }
}
