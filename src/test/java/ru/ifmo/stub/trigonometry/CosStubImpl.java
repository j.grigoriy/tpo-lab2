package ru.ifmo.stub.trigonometry;

import ru.ifmo.math.trigonometry.CosFunction;
import ru.ifmo.stub.StubTables;

public class CosStubImpl implements CosFunction {
    @Override
    public Double cos(Double x) {
        return StubTables.cosArgToValue.get(x);
    }
}
