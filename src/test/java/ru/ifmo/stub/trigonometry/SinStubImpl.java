package ru.ifmo.stub.trigonometry;

import ru.ifmo.math.trigonometry.SinFunction;
import ru.ifmo.stub.StubTables;

public class SinStubImpl implements SinFunction {
    @Override
    public Double sin(Double x) {
        return StubTables.sinArgToValue.get(x);
    }
}
