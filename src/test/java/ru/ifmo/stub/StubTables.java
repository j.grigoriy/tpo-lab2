package ru.ifmo.stub;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StubTables {
    public static final List<Double> trigonometryArguments = new ArrayList<Double>() {{
        add(-10 * Math.PI / 3);
        add(-10 * Math.PI / 3 + Math.PI / 2);
        add(-Math.PI / 4);
        add(-Math.PI / 4 + Math.PI / 2);
        add(-2.28 * Math.PI / 99);
        add(-2.28 * Math.PI / 99 + Math.PI / 2);
        add(-666 * Math.PI / 133);
        add(-666 * Math.PI / 133 + Math.PI / 2);
        add(-Math.PI / 8);
        add(-Math.PI / 8 + Math.PI / 2);
    }};

    public static final List<Double> logarithmArguments = new ArrayList<Double>() {{
        add(0.8);
        add(1.0);
        add(1.2);
        add(2.28);
        add(2.0);
        add(3.0);
        add(10.0);
        add(Math.E);
    }};

    public static final Map<Double, Double> sinArgToValue = trigonometryArguments.stream().collect(Collectors.toMap(x -> x, Math::sin));
    public static final Map<Double, Double> cosArgToValue = trigonometryArguments.stream().collect(Collectors.toMap(x -> x, Math::cos));
    public static final Map<Double, Double> tanArgToValue = trigonometryArguments.stream().collect(Collectors.toMap(x -> x, Math::tan));
    public static final Map<Double, Double> secArgToValue = trigonometryArguments.stream().collect(Collectors.toMap(x -> x, x -> 1 / Math.cos(x)));
    public static final Map<Double, Double> cscArgToValue = trigonometryArguments.stream().collect(Collectors.toMap(x -> x, x -> 1 / Math.sin(x)));

    public static final Map<Double, Double> lnArgToValue = logarithmArguments.stream().collect(Collectors.toMap(x -> x, Math::log));
    public static final Map<Double, Double> log2ArgToValue = logarithmArguments.stream().collect(Collectors.toMap(x -> x, x -> Math.log(x) / Math.log(2)));
    public static final Map<Double, Double> log3ArgToValue = logarithmArguments.stream().collect(Collectors.toMap(x -> x, x -> Math.log(x) / Math.log(3)));
    public static final Map<Double, Double> log10ArgToValue = logarithmArguments.stream().collect(Collectors.toMap(x -> x, x -> Math.log(x) / Math.log(10)));
}
