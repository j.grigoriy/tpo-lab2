package ru.ifmo.math.logarithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LnFunctionImplTest {

    private static final double DELTA = 0.001;

    static Stream<Double> getNormalDataSet() {
        return Stream.of(
                1.0,
                Math.E,
                Math.pow(Math.E, -1),
                Math.E * 10,
                10.0,
                25.0
        );
    }

    static Stream<Double> getExceptionDataSet() {
        return Stream.of(
                -Math.E,
                -1.0,
                -0.0000001,
                Double.NaN,
                Double.POSITIVE_INFINITY,
                Double.NEGATIVE_INFINITY
        );
    }


    @ParameterizedTest
    @MethodSource("getNormalDataSet")
    void lnTests(Double x) {
        LnFunction lnFunction = new LnFunctionImpl();
        assertEquals(Math.log(x), lnFunction.ln(x), DELTA);
    }

    @ParameterizedTest
    @MethodSource("getExceptionDataSet")
    void lnExceptionTests(Double x) {
        LnFunction lnFunction = new LnFunctionImpl();
        assertThrows(IllegalArgumentException.class, () -> lnFunction.ln(x));
    }
}