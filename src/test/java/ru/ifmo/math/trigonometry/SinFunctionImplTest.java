package ru.ifmo.math.trigonometry;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SinFunctionImplTest {

    private static final double DELTA = 0.001;

    static Stream<Double> getDataSet() {
        return Stream.of(
                Math.PI,
                Math.PI/4,
                28*Math.PI/3,
                -28*Math.PI/3,
                Math.pow(Math.PI, -1),
                Math.PI * 10,
                10.0,
                25.0
        );
    }

    static Stream<Double> getExceptionDataSet() {
        return Stream.of(
                Double.NaN,
                Double.POSITIVE_INFINITY,
                Double.NEGATIVE_INFINITY
        );
    }

    @ParameterizedTest
    @MethodSource("getDataSet")
    void sinTests(Double x) {
        SinFunction sinFunction = new SinFunctionImpl();
        assertEquals(Math.sin(x), sinFunction.sin(x), DELTA);
    }

    @ParameterizedTest
    @MethodSource("getExceptionDataSet")
    void sinExceptionTests(Double x) {
        SinFunction sinFunction = new SinFunctionImpl();
        assertThrows(IllegalArgumentException.class, () -> sinFunction.sin(x));
    }
}